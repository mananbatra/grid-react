import React, { useState } from 'react';
import './App.css';

function App() {
  const [rows, setRows] = useState(0);
  const [cols, setCols] = useState(0);
  const [grid, setGrid] = useState([]);

  const generateGrid = () => {
    let counter = 1;
    let newGrid = [];

    for (let i = 0; i < rows; i++) {
      let row = [];
      for (let j = 0; j < cols; j++) {
        if (i % 2 === 0) {
          row.push(counter++);
        } else {
          row.unshift(counter++);
        }
      }
      newGrid.push(row);
    }

    setGrid(newGrid);
    console.log(newGrid)
  };


  return (
    <div className="App">
      <div className="App">
        <input
          type="number"
          placeholder="Rows"
          value={rows}
          onChange={(e) => setRows(e.target.value)}
        />
        <input
          type="number"
          placeholder="Columns"
          value={cols}
          onChange={(e) => setCols(e.target.value)}
        />
        <button onClick={generateGrid}>Generate Grid</button>
      </div>

      <div className="grid">
        {grid.map((row, rowIndex) => (
          <div key={rowIndex} className="row">
            {row.map((cell, cellIndex) => (
              <div key={cellIndex} className="cell">
                {cell}
              </div>
            ))}
          </div>
        ))}
      </div>
    </div>
  );
}

export default App;